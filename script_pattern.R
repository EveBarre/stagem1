#Script de formation des profils à partir des comptes d'expression 

#1) Données de RNASeq
Counts_tt <- counts(dds_f, normalized = T)

#Filt_DE est la liste des gènes dont l'expression varie significativement
#entre au moins deux types cellulaires.

#Sélection des gènes qui sont dans Filt_DE
Counts_tt_DE <- Counts_tt[which(rownames(Counts_tt) %in% Filt_DE),]
#attention, peut-être rajouter les gènes 55555(invariants) car peuvent
#être des TF influencant le réseau de régulation 

#ordre des type cell : CB, CC, FL, NBC, PB

#Table des log2 des comptes moyens par type cellulaire
Mean_counts <- data.frame("CB" = mean(Counts_tt_DE[1, 1:4]),
                          "CC" = mean(Counts_tt_DE[1, 5:8]),
                          "FL" = mean(Counts_tt_DE[1, 9:22]),
                          "NBC" = mean(Counts_tt_DE[1, 23:25]),
                          "PB" = mean(Counts_tt_DE[1, 26:28]))

for (i in 2:nrow(Counts_tt_DE)){
  CurrentRow <- data.frame("CB" = mean(Counts_tt_DE[i, 1:4]),
                           "CC" = mean(Counts_tt_DE[i, 5:8]),
                           "FL" = mean(Counts_tt_DE[i, 9:22]),
                           "NBC" = mean(Counts_tt_DE[i, 23:25]),
                           "PB" = mean(Counts_tt_DE[i, 26:28]))
  Mean_counts <- rbind(Mean_counts, CurrentRow)
}

rownames(Mean_counts) <- ("Gene" = Filt_DE)

Mean_counts_1 <- Mean_counts +0.1

Mean_counts_log <- log(Mean_counts_1, 2)

#Fonction attribuant le chiffre du profil selon le compte.
ExpPatt = function(Exp, Max, Min){
  pas <- (Max - Min) / 4
  if (Exp <= Max && Exp > (Max - pas)){
    Nx <- 4
  }else if (Exp > (Max - (2*pas))){
    Nx <- 3
  }else if (Exp > (Max - (3*pas))){
    Nx <- 2
  }else {
    Nx <- 1
  }
}


#Table de Profils : 

MaxR <- max(Mean_counts_log[1,])
MinR <- min(Mean_counts_log[1,])

NxCB <- ExpPatt(Exp = Mean_counts_log[1, 1], Max = MaxR , Min= MinR)
NxCC <- ExpPatt(Exp = Mean_counts_log[1, 2], Max = MaxR, Min= MinR)
NxFL <- ExpPatt(Exp = Mean_counts_log[1, 3], Max = MaxR, Min= MinR)
NxNBC <- ExpPatt(Exp = Mean_counts_log[1, 4], Max = MaxR, Min= MinR)
NxPB <- ExpPatt(Exp = Mean_counts_log[1, 5], Max = MaxR, Min= MinR)
NxPatt <- paste(NxCB, NxCC, NxFL, NxNBC, NxPB, sep = "")

Patt <- data.frame("CB" = NxCB,
                   "CC" = NxCC,
                   "FL" = NxFL,
                   "NBC" = NxNBC,
                   "PB" = NxPB,
                   "ProfileExpr" = NxPatt)

for (i in 2:length(Filt_DE)){
  MaxR <- max(Mean_counts_log[i,])
  MinR <- min(Mean_counts_log[i,])

  NxCB <- ExpPatt(Exp = Mean_counts_log[i, 1], Max = MaxR , Min= MinR)
  NxCC <- ExpPatt(Exp = Mean_counts_log[i, 2], Max = MaxR, Min= MinR)
  NxFL <- ExpPatt(Exp = Mean_counts_log[i, 3], Max = MaxR, Min= MinR)
  NxNBC <- ExpPatt(Exp = Mean_counts_log[i, 4], Max = MaxR, Min= MinR)
  NxPB <- ExpPatt(Exp = Mean_counts_log[i, 5], Max = MaxR, Min= MinR)
  NxPatt <- paste(NxCB, NxCC, NxFL, NxNBC, NxPB, sep = "")
  
  CRow <- data.frame("CB" = NxCB,
                     "CC" = NxCC,
                     "FL" = NxFL,
                     "NBC" = NxNBC,
                     "PB" = NxPB,
                     "ProfileExpr" = NxPatt)

  Patt <- rbind(Patt, CRow)
}

rownames(Patt) <- Filt_DE

write.table(as.data.frame(Patt), file = "gene_pattern.csv", sep = ",", quote = F)


#Deux méthodes pour la répartission des valeurs avec la fonction ExPatt selon l'inclusion 
#(commencer par >= ou bien > : Patt1 et Patt 2), vérification si les tables générées sont modifiées : 
Diff2_Patt1_2 <- Patt2[which(Patt2 != Patt1), ]
#table vide, pas de différence entre les deux méthodes sur les patterns de sortie

#____________Essais autres méthodes à comparer sur les résultats de sortie du pipeline____________
#_________________________________________________________________________________________________

#Application avec les données transformés par DESeq:
#Deux fonctions peuvent être utilisées : 
varianceStabilizingTransformation() 
rlogTransformation()

ValeursT <- assay(vsd_f)

ValeursT_DE <- ValeursT[which(rownames(ValeursT) %in% Filt_DE),]

#table des moyennes

Mean_ValeursT <- data.frame("CB" = mean(ValeursT_DE[1, 1:4]),
                          "CC" = mean(ValeursT_DE[1, 5:8]),
                          "FL" = mean(ValeursT_DE[1, 9:22]),
                          "NBC" = mean(ValeursT_DE[1, 23:25]),
                          "PB" = mean(ValeursT_DE[1, 26:28]))

for (i in 2:nrow(ValeursT_DE)){
  CRow <- data.frame("CB" = mean(ValeursT_DE[i, 1:4]),
                           "CC" = mean(ValeursT_DE[i, 5:8]),
                           "FL" = mean(ValeursT_DE[i, 9:22]),
                           "NBC" = mean(ValeursT_DE[i, 23:25]),
                           "PB" = mean(ValeursT_DE[i, 26:28]))
  Mean_ValeursT <- rbind(Mean_ValeursT, CRow)
}

rownames(Mean_ValeursT) <- Filt_DE

#Table Pattern valeurs transformées : 
MaxR <- max(Mean_ValeursT[1,])
MinR <- min(Mean_ValeursT[1,])

NxCB <- ExpPatt(Exp = Mean_ValeursT[1, 1], Max = MaxR , Min= MinR)
NxCC <- ExpPatt(Exp = Mean_ValeursT[1, 2], Max = MaxR, Min= MinR)
NxFL <- ExpPatt(Exp = Mean_ValeursT[1, 3], Max = MaxR, Min= MinR)
NxNBC <- ExpPatt(Exp = Mean_ValeursT[1, 4], Max = MaxR, Min= MinR)
NxPB <- ExpPatt(Exp = Mean_ValeursT[1, 5], Max = MaxR, Min= MinR)

Patt2_VT <- data.frame("CB" = NxCB,
                   "CC" = NxCC,
                   "FL" = NxFL,
                   "NBC" = NxNBC,
                   "PB" = NxPB)

for (i in 2:length(Filt_DE)){
  MaxR <- max(Mean_ValeursT[i,])
  MinR <- min(Mean_ValeursT[i,])
  
  NxCB <- ExpPatt(Exp = Mean_ValeursT[i, 1], Max = MaxR , Min= MinR)
  NxCC <- ExpPatt(Exp = Mean_ValeursT[i, 2], Max = MaxR, Min= MinR)
  NxFL <- ExpPatt(Exp = Mean_ValeursT[i, 3], Max = MaxR, Min= MinR)
  NxNBC <- ExpPatt(Exp = Mean_ValeursT[i, 4], Max = MaxR, Min= MinR)
  NxPB <- ExpPatt(Exp = Mean_ValeursT[i, 5], Max = MaxR, Min= MinR)
  
  CRow <- data.frame("CB" = NxCB,
                     "CC" = NxCC,
                     "FL" = NxFL,
                     "NBC" = NxNBC,
                     "PB" = NxPB)
  Patt2_VT <- rbind(Patt2_VT, CRow)
}

rownames(Patt2_VT) <- Filt_DE

write.table(as.data.frame(Patt2_VT), file = "Table_Patt_RNA_VT.txt")


#Comparaison avec Patt_VT(données transformées par DESeq2) et Patt (counts):
DiffRNA_PattVT_counts <- Patt_VT[which(Patt2_VT != Patt), ]

Patt1_VT <- Patt_VT
Diff1_PattVT <- Patt1_VT[which(Patt1_VT != Patt2_VT), ]
#Pas de différence non plus entre les deux tables générées selon les deux possibilités de la fonction ExpPatt

#_______________________________________________________________

#Profils pour les données ATACseq : selon les densités (coln°6), les régions (coln°4)

Table <- read.table("CB1_ATAC_countsAllRegNewN.txt", header = F)
Table_Densite <- data.frame("CB" = Table[,6])

for (CEchant in colnames(dds_ATAC)[2:26]){
  Table <- read.table(paste(CEchant,"_ATAC_countsAllRegNewN.txt", sep =""), header = F)
  Table_Densite <- cbind(Table_Densite, Table[,6])
}
rownames(Table_Densite) <- Table[,4]
colnames(Table_Densite) <- colnames(dds_ATAC)

#bien filtrer aussi selon les régions d'activité significativement différente

Table_Densite_DE <- Table_Densite[which(rownames(Table_Densite) %in% Filt_DE_ATAC), ]

nrow(Table_Densite)
nrow(Table_Densite_DE)
length(Filt_DE_ATAC)

Table_Densite_DE <- as.matrix(Table_Densite_DE)

#Table_Moyenne_densite 

Mean_densite <- data.frame("CB" = mean(Table_Densite_DE[1, 1:4]),
                          "CC" = mean(Table_Densite_DE[1, 5:8]),
                          "FL" = mean(Table_Densite_DE[1, 9:22]),
                          "NBC" = mean(Table_Densite_DE[1, 23]),
                          "PB" = mean(Table_Densite_DE[1, 24:26]))

for (i in 2:nrow(Table_Densite_DE)){
  CurrentR <- data.frame("CB" = mean(Table_Densite_DE[i, 1:4]),
                           "CC" = mean(Table_Densite_DE[i, 5:8]),
                           "FL" = mean(Table_Densite_DE[i, 9:22]),
                           "NBC" = mean(Table_Densite_DE[i, 23]),
                           "PB" = mean(Table_Densite_DE[i, 24:26]))
  Mean_densite <- rbind(Mean_densite, CurrentR)
}

rownames(Mean_densite) <- Filt_DE_ATAC

Mean_densite_1 <- Mean_densite +(1e-7)

Mean_densite_log <- log(Mean_densite_1, 2)

#Table de profils ATACseq

MaxR <- max(Mean_densite_log[1,])
MinR <- min(Mean_densite_log[1,])

NxCB <- ExpPatt(Exp = Mean_densite_log[1, 1], Max = MaxR , Min= MinR)
NxCC <- ExpPatt(Exp = Mean_densite_log[1, 2], Max = MaxR, Min= MinR)
NxFL <- ExpPatt(Exp = Mean_densite_log[1, 3], Max = MaxR, Min= MinR)
NxNBC <- ExpPatt(Exp = Mean_densite_log[1, 4], Max = MaxR, Min= MinR)
NxPB <- ExpPatt(Exp = Mean_densite_log[1, 5], Max = MaxR, Min= MinR)
NxPatt <- paste(NxCB, NxCC, NxFL, NxNBC, NxPB, sep = "")


Patt_ATACseq <- data.frame("CB" = NxCB,
                   "CC" = NxCC,
                   "FL" = NxFL,
                   "NBC" = NxNBC,
                   "PB" = NxPB,
                   "ProfileExpr" = NxPatt)

for (i in 2:length(Filt_DE_ATAC)){
  MaxR <- max(Mean_densite_log[i,])
  MinR <- min(Mean_densite_log[i,])
  
  NxCB <- ExpPatt(Exp = Mean_densite_log[i, 1], Max = MaxR , Min= MinR)
  NxCC <- ExpPatt(Exp = Mean_densite_log[i, 2], Max = MaxR, Min= MinR)
  NxFL <- ExpPatt(Exp = Mean_densite_log[i, 3], Max = MaxR, Min= MinR)
  NxNBC <- ExpPatt(Exp = Mean_densite_log[i, 4], Max = MaxR, Min= MinR)
  NxPB <- ExpPatt(Exp = Mean_densite_log[i, 5], Max = MaxR, Min= MinR)
  NxPatt <- paste(NxCB, NxCC, NxFL, NxNBC, NxPB, sep = "")
  
  CRow <- data.frame("CB" = NxCB,
                     "CC" = NxCC,
                     "FL" = NxFL,
                     "NBC" = NxNBC,
                     "PB" = NxPB,
                     "ProfileExpr" = NxPatt)
  
  Patt_ATACseq <- rbind(Patt_ATACseq, CRow)
}

rownames(Patt_ATACseq) <- Filt_DE_ATAC

write.table(as.data.frame(Patt_ATACseq), file = "region_pattern.csv", sep = ",", quote = F)


#_______________________________________________________________

#Profils aussi sur les counts pour les données ATACseq :
Counts_ATAC <- counts(dds_ATAC, normalized = T)
#Sélection des gènes qui sont dans Filt_DE
Counts_ATAC_DE <- Counts_ATAC[which(rownames(Counts_ATAC) %in% Filt_DE_ATAC),]

#ordre des type cell : CB, CC, FL, NBC, PB

Mean_counts_ATAC <- data.frame("CB" = mean(Counts_ATAC_DE[1, 1:4]),
                          "CC" = mean(Counts_ATAC_DE[1, 5:8]),
                          "FL" = mean(Counts_ATAC_DE[1, 9:22]),
                          "NBC" = mean(Counts_ATAC_DE[1, 23]),
                          "PB" = mean(Counts_ATAC_DE[1, 24:26]))

for (i in 2:nrow(Counts_ATAC_DE)){
  CurrentRow <- data.frame("CB" = mean(Counts_ATAC_DE[i, 1:4]),
                           "CC" = mean(Counts_ATAC_DE[i, 5:8]),
                           "FL" = mean(Counts_ATAC_DE[i, 9:22]),
                           "NBC" = mean(Counts_ATAC_DE[i, 23]),
                           "PB" = mean(Counts_ATAC_DE[i, 24:26]))
  Mean_counts_ATAC <- rbind(Mean_counts_ATAC, CurrentRow)
}

rownames(Mean_counts_ATAC) <- Filt_DE_ATAC

Mean_counts_ATAC_1 <- Mean_counts_ATAC + 1

Mean_counts_ATAC_log <- log(Mean_counts_ATAC_1, 2)


MaxR <- max(Mean_counts_ATAC_log[1,])
MinR <- min(Mean_counts_ATAC_log[1,])

NxCB <- ExpPatt(Exp = Mean_counts_ATAC_log[1, 1], Max = MaxR , Min= MinR)
NxCC <- ExpPatt(Exp = Mean_counts_ATAC_log[1, 2], Max = MaxR, Min= MinR)
NxFL <- ExpPatt(Exp = Mean_counts_ATAC_log[1, 3], Max = MaxR, Min= MinR)
NxNBC <- ExpPatt(Exp = Mean_counts_ATAC_log[1, 4], Max = MaxR, Min= MinR)
NxPB <- ExpPatt(Exp = Mean_counts_ATAC_log[1, 5], Max = MaxR, Min= MinR)

Patt_counts_ATAC <- data.frame("CB" = NxCB,
                   "CC" = NxCC,
                   "FL" = NxFL,
                   "NBC" = NxNBC,
                   "PB" = NxPB)

for (i in 2:length(Filt_DE_ATAC)){
  MaxR <- max(Mean_counts_ATAC_log[i,])
  MinR <- min(Mean_counts_ATAC_log[i,])
  
  NxCB <- ExpPatt(Exp = Mean_counts_ATAC_log[i, 1], Max = MaxR , Min= MinR)
  NxCC <- ExpPatt(Exp = Mean_counts_ATAC_log[i, 2], Max = MaxR, Min= MinR)
  NxFL <- ExpPatt(Exp = Mean_counts_ATAC_log[i, 3], Max = MaxR, Min= MinR)
  NxNBC <- ExpPatt(Exp = Mean_counts_ATAC_log[i, 4], Max = MaxR, Min= MinR)
  NxPB <- ExpPatt(Exp = Mean_counts_ATAC_log[i, 5], Max = MaxR, Min= MinR)
  
  CRow <- data.frame("CB" = NxCB,
                     "CC" = NxCC,
                     "FL" = NxFL,
                     "NBC" = NxNBC,
                     "PB" = NxPB)
  Patt_counts_ATAC <- rbind(Patt_counts_ATAC, CRow)
}

rownames(Patt_counts_ATAC) <- Filt_DE_ATAC

write.table(as.data.frame(Patt_counts_ATAC), file = "Table_Patt_counts_ATAC.csv")

